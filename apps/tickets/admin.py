from django.contrib import admin
from .models import ManagerTickets


# Register your models here.
@admin.register(ManagerTickets)
class ManagerTicketsAdmin(admin.ModelAdmin):
    search_fields = [
        'file_to_upload',
    ]

    list_display = [
        'id',
        'file_to_upload',
        'user_upload',
    ]

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser

    def has_view_permission(self, request, obj=None):
        return request.user.is_superuser
