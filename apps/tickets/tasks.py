from celery import shared_task
import cloudinary.api
import cloudinary.uploader


def upload_file_to_server(file_path):
    result = cloudinary.uploader.upload(file_path, upload_preset="my_upload_preset")
    return result['secure_url']


@shared_task()
def task_upload_file_to_server():
    upload_preset_name = "my_preset"
    upload_preset_options = {
        "unsigned": False,
        "folder": "my_folder",
        "tags": "my_tags",
        "transformation": [
            {"width": 500, "height": 500, "crop": "fill"},
            {"effect": "grayscale"},
        ],
        "categorization": "aws_rek_tagging",
        "auto_tagging": 0.9
    }

    # Create the upload preset using the SDK
    upload_preset = cloudinary.api.create_upload_preset(
        name=upload_preset_name,
        settings=upload_preset_options
    )
    # Check if the upload preset was created successfully
    if upload_preset.get("name") == upload_preset_name:
        print("Upload preset created successfully.")
    else:
        print("Failed to create upload preset.")
