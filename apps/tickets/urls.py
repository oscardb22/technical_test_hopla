from rest_framework import routers
from .views import ManagerTicketsViewSet

router_api_rest = routers.DefaultRouter(trailing_slash=True)

router_api_rest.register(r'tickets', ManagerTicketsViewSet)
urlpatterns = router_api_rest.urls
