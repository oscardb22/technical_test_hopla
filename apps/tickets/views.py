from rest_framework.viewsets import GenericViewSet
from rest_framework import mixins
from rest_framework.status import HTTP_201_CREATED
from rest_framework.response import Response
from .models import ManagerTickets
from .tasks import task_upload_file_to_server, upload_file_to_server


# Create your views here.
class ManagerTicketsViewSet(GenericViewSet, mixins.CreateModelMixin):
    """docstring for ManagerTicketsViewSet"""

    queryset = ManagerTickets.objects.all()
    permission_classes = ()
    http_method_names = ('post', )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        task_upload_file_to_server.delay(serializer.file_to_upload)
        upload_file_to_server(serializer.file_to_upload)
        return Response(serializer.data, status=HTTP_201_CREATED, headers=headers)
