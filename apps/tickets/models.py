from django.db import models
from django.utils.translation import gettext_lazy as _
from apps.user.models import User


class ManagerTickets(models.Model):
    user_upload = models.ForeignKey(
        User,
        models.SET_NULL,
        verbose_name=_("content type"),
        blank=True,
        null=True,
    )
    file_to_upload = models.FileField()

    def __str__(self):
        return f'[{self.file_to_upload.name}'
