from rest_framework import serializers
from apps.user.models import User


class UserSerializer(serializers.ModelSerializer):
    """"""
    first_name = serializers.CharField(required=True, allow_blank=False, allow_null=False)
    last_name = serializers.CharField(required=True, allow_blank=False, allow_null=False)

    class Meta:
        model = User
        fields = (
            'id', 'email', 'first_name', 'last_name', 'password'
        )
        read_only_fields = ('id',)
