from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST
from rest_framework.viewsets import GenericViewSet
from rest_framework import mixins
from .models import User
from .serializer import (
    UserSerializer
)


class UserViewSet(GenericViewSet, mixins.CreateModelMixin):
    """docstring for UserViewSet"""

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = ()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = User.objects.create_user(**serializer.validated_data)
        user.set_password(serializer.validated_data.get("password"))
        user.is_active = False
        user.save()
        if user:
            serialized_user = UserSerializer(user, context={"request": request})
            return Response(serialized_user.data, status=HTTP_201_CREATED)


def user_email_exist(request):
    email = request.GET.get("email")
    if not email:
        return Response(
            {"error": "Please send an email address"}, status=HTTP_400_BAD_REQUEST
        )
    return JsonResponse({"exists": User.objects.filter(email=email).exists()})

