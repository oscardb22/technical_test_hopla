from django.urls import path
from rest_framework import routers

from apps.user.views import UserViewSet
from . import views

router_api_rest = routers.DefaultRouter(trailing_slash=True)

router_api_rest.register(r'user', UserViewSet)
urlpatterns = router_api_rest.urls
