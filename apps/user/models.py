from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone, formats
from django.core.mail import send_mail
from django.contrib.auth.models import (BaseUserManager, AbstractBaseUser, PermissionsMixin)
from django.contrib.auth.models import Group

Group.add_to_class('menu', models.JSONField(null=True, blank=True))
Group.add_to_class('email_authorizing_user', models.EmailField(null=True, blank=True, default=None))


class UserManager(BaseUserManager):
    """docstring for UserManager"""

    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(
            email=email, is_staff=is_staff, is_active=True, is_superuser=is_superuser, date_joined=now, **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True, **extra_fields)

    def create_staff(self, email, password, **extra_fields):
        return self._create_user(email, password, True, False, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """docstring for User"""
    is_staff = models.BooleanField(
        _('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin site.')
    )
    is_active = models.BooleanField(
        _('active'), default=True,
        help_text=_(
            'Designates whether this user should be treated as active. Unselect this instead of deleting accounts.')
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    email = models.EmailField(_('email address'), unique=True, )
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    address = models.CharField(
        _('Address'), max_length=200, db_column='address', help_text=_('Address'), blank=True, null=True)

    authorizing_user = models.CharField(_('authorizing user'), max_length=50, blank=True, null=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __human__(self):
        object_json = {
            'date_joined':
                u'%s' % formats.date_format(
                    self.date_joined),
            'last_login':
                u'%s' % formats.date_format(
                    self.last_login),
        }
        return object_json

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    @staticmethod
    def operation_emails():
        return list(User.objects.filter(
            is_active=True,
            groups__name__iexact='staff'
        ).values_list('email', flat=True))

    @staticmethod
    def invoicing_emails():
        return list(User.objects.filter(
            is_active=True,
            groups__name__iexact='invoicing'
        ).values_list('email', flat=True))

    @staticmethod
    def exchange_rate_emails():
        return list(User.objects.filter(
            is_active=True,
            groups__name__iexact='exchange_rate'
        ).values_list('email', flat=True))

    class Meta:
        app_label = 'user'
        db_table = 'user_users'
        ordering = ['email', 'first_name', 'last_name']
        verbose_name = _("user")
        verbose_name_plural = _("users")
