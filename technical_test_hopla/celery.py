from celery import Celery
from celery.schedules import crontab
from django.conf import settings
import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'technical_test_hopla.settings')
app = Celery('technical_test_hopla')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda: settings.APPS)


# @app.on_after_finalize.connect
# def setup_periodic_tasks(sender, **kwargs):
#     # Calls test('hello') every 10 seconds.
#     sender.add_periodic_task(crontab(0, 0, day_of_month='1'), task_monthly_charge.s(), name='generate monthly charge')

app.conf.beat_schedule = {
    'synchronize-data': {
        'task': 'apps.integrations.tasks.synchronize_data',
        'schedule': crontab(hour='*/6'),  # Run every 6 hours
    },
}

app.conf.timezone = 'UTC'
